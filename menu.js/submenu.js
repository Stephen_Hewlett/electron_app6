let fb = document.getElementById('fb');
let tw = document.getElementById('tw');
let yt = document.getElementById('yt');

fb.addEventListener('click', () => {
    goToPage('https://facebook.com');
})

tw.addEventListener('click', () => {
    goToPage('https://twitter.com');
})

yt.addEventListener('click', () => {
    goToPage('https://youtube.com');
})

let goToPage = (url) => {
    const electron = require('electron')
    const remote = electron.remote
    const browser = remote.BrowserWindow

    let subwin = new browser({frame: true, titlebar: 'hidden', width: 800, height: 600})
    subwin.loadURL(url)

    window.addEventListener('contextmenu', (e) => {
        e.preventDefault()
        subwin.popup(remote.getCurrentWindow())
    }, false)
} 